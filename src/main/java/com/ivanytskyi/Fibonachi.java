package com.ivanytskyi;
/**робимо цикл для формування порядку фібоначі */
public class Fibonachi {
    static void fibo(int a, int b, int c) {
        int f3;
        int[] fiboArr = new int[c + 2];
        fiboArr[0] = a;
        fiboArr[1] = b;

        //    System.out.print(a + " " + b + " ");
        for (int i = a, x = 2; i <= c; i++, x++) {
            f3 = a + b;
            //     System.out.print(f3 + " ");
            fiboArr[x] = f3;
            a = b;
            b = f3;
        }
        System.out.println("Порядок Фібоначчі:");
        for (int element : fiboArr) {
            System.out.print(element + " ");
        }

/**далі цикл для підрахунку парних і непарних чисел
 * та процентне значення */
        int odd = 0; // змінна з кількістю непарних чисел фіб
        int even = 0; // змінна з кількістю парних чисел фіб

        System.out.println("");
        System.out.print("Парні элементи массива fiboArr: ");
        for (int i = 0; i < fiboArr.length; i++) {
            if (fiboArr[i] % 2 == 0) {
                even++;
                System.out.print(fiboArr[i] + " ");
            }
        }
        double evenPercent = (even*100)/fiboArr.length;
        System.out.println("Кількість парних чисел: " + even);
        System.out.print("Процент парних чисел: " + evenPercent +" %");

        // Вывод нечетных элементов массива на экран.
        System.out.println("");
        System.out.print("\nНепарні элементи массива fiboArr: ");
        for (int i = 0; i < fiboArr.length; i++) {
            if (fiboArr[i] % 2 != 0) {
                odd++;
                System.out.print(fiboArr[i] + " ");
            }
        }
        double oddPercent = (odd*100)/fiboArr.length;
        System.out.println("Кількість непарних чисел: " + odd);
        System.out.print("Процент непарних чисел: " + oddPercent +" %");
    }
}